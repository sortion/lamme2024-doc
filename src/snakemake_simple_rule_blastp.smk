rule blastp:
    input:
        query="{name}.fasta",
        db=[...]
    output:
        tsv="{name}.blastp.tsv"
    params:
        db="db/{name}",
        matrix="BLOSUM62"
    shell:
        """blastp -query "{input.query}" -db "{params.db}" \
            -outfmt "{params.format}" -out "{output.tsv}" \
            -matrix {params.matrix}
        """
