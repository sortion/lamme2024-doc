from pygments.lexer import bygroups, RegexLexer
from pygments.lexers.python import PythonLexer
from pygments.token import *

class SnakemakeLexer(PythonLexer):
    name = "Snakemake"
    aliases = ["smk"]
    filenames = ["Snakefile", "*.smk"]
    tokens = {
        'root': [
            (r'(rule)((?:\s|\\\s)+)', bygroups(Keyword, Text)),
            (r'(input|output|conda|shell|script|params|log|run|)(:)', bygroups(Keyword, Text)),
        ],
    }

__all__ = ["SnakemakeLexer"]
