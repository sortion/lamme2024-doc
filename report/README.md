# lamme2024-report

LaTeX source code for my report for my  M1 internship at the _Laboratoire de Mathématiques et Modélisation d'Évry_ under the supervision of Carène Rizzon and Franck Samson on duplicate genes and tandemly arrayed genes.