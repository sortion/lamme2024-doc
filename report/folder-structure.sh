#!/bin/sh

find ./content -type d > folder_list.txt

mkdir -p build
cd build
cat ../folder_list.txt | xargs mkdir -p
rm ../folder_list.txt