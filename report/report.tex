% M1 internship report
% Author: Samuel Ortion
% License: CC-BY-SA 4.0

\documentclass{scrreprt}
\usepackage{sty/lamme2024}
\usepackage{minted}

\title{Further development on FTAG-Finder}
\subtitle{a pipeline to identify Gene Families and Tandemly Arrayed Genes}
\author{Samuel Ortion}
\date{\today}
\newcommand{\keywords}{duplicate genes, tandemly arrayed genes, pipeline}

\makeatletter
\hypersetup{
	pdfauthor={Samuel Ortion},
	pdftitle={\@title},
	pdfkeywords={\keywords},
}
\makeatother
\addbibresource{../references.bib}
\makeindex
\makeglossaries%

% \includeonly{content/introduction}

\begin{document}

\input{glossaries}

\include{titlepage}

\chapter{Acknowledgement}

I would like to thank my supervisors, Mrs Carène Rizzon and Mr Franck Samson for their support during the internship. Their expertises in comparative genomics and technical tools used in my internship proved to be especially useful throughout my work.

I would like to express my gratitude to every members of the Laboratoire de Mathématiques et Modélisation d'Évry for their warm welcome, and for the numerous discussion we had in lunch time.

In particular, I'm grateful for the warm welcome I received from my office neighbors, with whom we were able to discuss a number of subjects related to our interests and research.

Finally, I would like to thank my reviewer, Mr Ramzi Temmanni, for his proofreading.


% \flstart % disable, at least temporarily.

\tableofcontents
\clearpage
\begin{relaxclearpage}

  \listoffigures%

	\listoftables%

	\listoflistings%

\end{relaxclearpage}

\clearpage

\printglossaries%
\chapter{Introduction}

\label{part:introduction}

\section{General notions on duplicate genes}

Duplicate genes represent an important fraction of Eukaryotic genes: It is estimated that between 46\% and 65.5\% of human genes could be considered as duplicates. The estimate varies strongly depending on the criteria in use, because ancient duplication event may be hard to detect \autocite{correaTransposableElementEnvironment2021}.

Back in the 70's, in his book \textit{Evolution by Gene Duplication}, Susumu Ohno describes how gene duplication is involved in species evolution \autocite{ohnoEvolutionGeneDuplication1970}.

Gene duplication increases genetic variability. It is also involved in some speciation events.

\subsection{Gene duplication mechanisms}

\begin{figure}
	\includegraphics[width=.9\linewidth]{../figures/lallemand2020-fig1_copy.pdf}
	\caption[Different types of duplication]{\label{fig:gene-duplication-mechanisms}Different types of duplications. (A) Whole genome duplication. (B) An unequal crossing-over leads to a duplication of a fragment of a chromosome. (C) In tandem duplication, two (set of) genes are duplicated one after the other. (D) Retrotransposons enable retroduplication: a RNA transcript is reverse transcribed and inserted back without introns and with a polyA tail in the genome. (E) A DNA transposons can acquire a fragment of a gene. (F) Segmental duplication corresponds to long stretches of duplicated sequences with high identity. (Adapted from \textcite{lallemandOverviewDuplicatedGene2020} (fig. 1)).}
  \end{figure}

Multiple mechanisms may lead to gene duplication. Their effects range from the duplication of the whole genome to the duplication of a fragment of a gene.

\subsubsection{Whole genome duplication and polyploidisation}
During an event of \gls{WGD}, the entire set of genes present on the chromosomes is duplicated (\cref{fig:gene-duplication-mechanisms} (A)).
\gls{WGD} can occur thanks to \gls{polyspermy} or in case of a non-reduced gamete.
A striking example is probably \emph{Triticum aestivum} (wheat) which is hexaploid due to hybridisation events \autocite{golovninaMolecularPhylogenyGenus2007a}.

We distinguish two kinds of \glspl{polyploidisation}, based on the origin of the duplicate genome: (i) \Gls{allopolyploidisation} occurs when the supplementary chromosomes come from a divergent species. This is the case for the \emph{Triticum aestivum} hybridisation, which consisted in the union of the chromosome set of a \emph{Triticum} species with that of an \emph{Aegilops} species. (ii) \Gls{autopolyploidisation} consists in the hybridisation or duplication of the whole genome within the same species.

\subsubsection{Unequal crossing-over}
Another source of gene duplication relies on unequal crossing-over. During cell division, a crossing-over occurs when two chromatids exchange a fragment of chromosome. If the cleavage of the two chromatids occurs at different positions, the shared fragments may have different lengths. Homologous recombination of such uneven crossing-over leads to the incorporation of a duplicate region, as depicted in \cref{fig:gene-duplication-mechanisms} (B, C).
This mechanism leads to the duplication of the whole set of genes present in the fragment. These duplicate genes locate one set after the other: we call them \gls{TAG}.

\subsubsection{Retroduplication}
Transposable elements play a major role in genome plasticity, and enable gene duplication too.
Retrotransposons, or RNA transposons are one type of transposable elements.
They share similar sequences structures and replication mechanisms with retroviruses.
Retrotransposons replicate in the genome through a mechanism known as \textquote{copy-and-paste}.
These transposons typically contain a reverse transcriptase gene. This enzyme proceeds in the reverse transcription of an mRNA transcript into its reverse complementary DNA sequence which can insert elsewhere in the genome.
More generally, \gls{retroduplication} refers to the duplication of a sequence through reverse transcription of a RNA transcript. Genes duplicated through retroduplication lose their intronic sequences and bring a polyA tail in their new locus (\cref{fig:gene-duplication-mechanisms} (D)).

\subsubsection{Transduplication}
DNA transposons are another kind of transposable elements whose transposition mechanism can also lead to gene duplication.
This type of transposable element moves in the genome through a mechanism known as \textquote{cut-and-paste}.
A typical DNA transposon contains a transposase gene. This enzyme recognizes two sites surrounding the donor transposon sequence in the chromosome resulting in a DNA cleavage and an excision of the transposon. Then, the transposase can insert the transposon at a new genome position. A transposon may bring a fragment of a gene during its transposition in the new locus (\cref{fig:gene-duplication-mechanisms} (E)), leading to the duplication of this fragment.

\subsubsection{Segment duplication}
Finally, \glspl{segment_duplication}, also called \emph{low copy repeats}, are long stretches of DNA with high identity score (\cref{fig:gene-duplication-mechanisms} (F)). Their exact duplication mechanism remains unclear \autocite{lallemandOverviewDuplicatedGene2020}. They may come from an accidental replication distinct from an uneven cross-over or from a double-stranded breakage.
Transposable elements may well be involved in the mechanism, as a high enrichment of transposable elements has been found next to duplicate segment extremities in \emph{Drosophila} \autocite{lallemandOverviewDuplicatedGene2020}.

\begin{figure}
	\includegraphics[width=.9\linewidth]{../figures/Evolution_fate_duplicate_genes.pdf}
	\caption[Fate of duplicate genes]{\label{fig:fate-duplicate-genes} Fate of duplicate genes. An original gene with four functions is duplicated. Its two copies may both keep the original functions (functional redoundancy). The original functions may split between the different copies (subfunctionalization). One of the copies may acquire a new function (neofunctionalization). It may also degenerate and lose its original functions (pseudogenization). (Adapted from \href{https://commons.wikimedia.org/wiki/File:Evolution_fate_duplicate_genes_-_vector.svg}{Smedlib}, \href{https://creativecommons.org/licenses/by-sa/4.0}{CC BY-SA 4.0}, via Wikimedia Commons.)}
\end{figure}

\subsection{Fate of duplicate genes in genome evolution}
Back in the 70s, in his book \emph{Evolution by Gene Duplication}, Susumu \textsc{Ohno} proposed that gene duplication plays a major role in species evolution \autocite{ohnoEvolutionGeneDuplication1970}. Indeed, gene duplication provides new genetic materials to build on a new phenotype while keeping a backup gene for the previous function.
Duplicate genes evolve after duplication: they may be inactivated, and become \glspl{pseudogene}, they may be deleted or conserved. If they are conserved, they may or may not acquire a new function.
\Cref{fig:fate-duplicate-genes} depicts the different possible fates of a duplicate gene.

\subsubsection{Pseudogenization}
As genome evolves, one duplicate gene may be inactivated and become pseudogene. This pseudogene keeps a gene-like structure which degrades as and when further genome modifications occur. Pseudogenes are no longer expressed.

\subsubsection{Neofunctionalization}
After duplication, the new gene copy may gain a new function.

\subsubsection{Subfunctionalization}
Two duplicate genes with the same original function may encounter a \gls{subfunctionalization}: each gene conserves only one part of the original function.

\subsubsection{Functional redoundancy}
Another possibility is that the two gene copies keep the ancestral function, resulting in a functional redoundancy. In this case the quantity of gene product may increase.

The study of \gls{TAG}s raises particular interest because they are overrepresented among duplicate genes. They play a role in species evolution in reaction to stress. In this period of climate change it is important to understand the mechanisms that enables plants to adapt to a changing environment \autocite{geryColdAcclimationDiversity2022}.
It could also enable the selection of plants of agronomic interest \autocite{DupliquerPourSadapter2020}.


\section{Existing tools}

Multiple tools targeting duplicate gene detection have been made available on the Internet, but a lot of them are not open source, and do not last after their home laboratory abandoned the maintenance of these web services. \autocite{lallemandEvolutionGenesDupliques2022} reviews these tools for each kind of duplicate genes.

Among these tools,
McScanX targets the detection of tandemly arrayed genes \cite{wangMCScanXToolkitDetection2012}. However, this tool does not construct gene families.

FTAG Finder is a tool developed in the LaMME laboratory that aims to detect gene families and extract \gls{TAG}s.

The aim of FTAG-Finder is to provide an open-source tool for fast analysis of families and tandemly arrayed genes on a single species proteome genome, providing multiple parameter choices to the user of the pipeline.

\section{Objectives for the internship}

My internship focuses on FTAG Finder.

\subsection{Scientific questions}
The underlying question of FTAG-Finder is the study of the evolutionary fate of duplicate genes in Eukaryotes.
The tool enables the detection of duplicate genes in the genome of a single species, and more specifically the detection of Tandemly Arrayed Genes.
For instance, the gene lists obtained from FTAG Finder could be used to answer questions such as ``What is the impact of gene duplication and \gls{TAG} on species evolution?'', ``Do genes belonging to a given TAG share the same orientation?''.
We might also get interested in the particular functions that genes belonging to a TAG preferentially perform.


\subsection{Extend the existing FTAG-Finder Galaxy pipeline}
Galaxy is a web-based platform for running accessible data analysis pipelines. It was first designed for use in genomics data analysis \autocite{goecksGalaxyComprehensiveApproach2010}.
Initially created in 2016, by Bérengère Bouillon, the Galaxy integration of the FTAG-Finder pipeline received multiple successive modifications by GENIOMHE interns, under the supervision of Carène Rizzon and Franck Samson \autocite{bouillonDevelopmentToolsIntegration2016,jasminStudyTandemlyArrayed2016,normandRapportV62017,charlesFinalisationPipelineFTAG2023}.
The legacy FTAG-Finder Galaxy versions are currently deployed on the server of the \emph{Laboratoire de Mathématiques et Modélisation d'Évry}\footnote{The galaxy instance is available through \url{http://stat.genopole.cnrs.fr/galaxy}}.

One objective of my internship is to pursue this work to further enhance the Galaxy version of FTAG-Finder, and to include new features.
Among these feature, a particular emphasis will be placed on adding (or fixing) tools to generate lists of gene pairs, building on top of work done by predecessors \autocite{kevinDevelopmentCreationListTool2017}.


\subsection{Port FTAG-Finder pipeline on Snakemake}
Another objective of my internship is to port FTAG-Finder on a workflow manager better suited to larger and more reproducible analysis.

We decided to use Snakemake.

Snakemake is a python-powered workflow manager based on rules \emph{à la} GNU Make \autocite{kosterSnakemakeScalableBioinformatics2012}.

\chapter{Material and methods}

\section{Data sources}

I ran the FTAG-Finder pipeline on the proteome of \textit{Arabidopsis thaliana}, \textit{Drosophila melanogaster} and \textit{Cyanistes caeruleus}.

\begin{itemize}
	\item \texttt{TAIR10} release of \textit{Arabidopsis thaliana} proteome and genomics features annotations, via Ensembl: \url{https://plants.ensembl.org/Arabidopsis_thaliana/Info/Index}
	      \begin{itemize}
		      \item \verb|Arabidopsis_thaliana.TAIR10.pep.all.fa.gz| -- protein sequences in compressed FASTA format
		      \item \verb|Arabidopsis_thaliana.TAIR10.59.gff3.gz| -- genomic features annotation in compressed \gls{GFF} format
	      \end{itemize}
  \item Dmel release \verb|r6.57| of \textit{Drosophila melanogaster} proteome and genomics features annotations, via Flybase: \url{http://ftp.flybase.net/releases/FB2024_02/dmel_r6.57/fasta/}
		\begin{itemize}
		  \item \verb|dmel-all-translation-r6.57.fasta.gz| -- protein sequences in compressed FASTA format
		  \item \verb|dmel-all-r6.57.gff.gz| -- genomic features annotation in compressed \gls{GFF} format
		\end{itemize}
	\item \texttt{cyaCae2} release of \textit{Cyanistes caeruleus} proteome and genomic features annotations, via Ensembl: \url{https://www.ensembl.org/Cyanistes_caeruleus/Info/Index}
	      \begin{itemize}
		      \item \verb|Cyanistes_caeruleus.cyaCae2.pep.all.fa.gz| -- protein sequences in compressed FASTA format
		      \item \verb|Cyanistes_caeruleus.cyaCae2.111.gff3.gz| -- genomic features annotation in compressed \gls{GFF} format
	      \end{itemize}
\end{itemize}

\section{Methods}

\subsection{The duplicate gene detection method used in FTAG Finder}

\begin{figure}
	\includegraphics[width=.9\linewidth]{../figures/tag-definition.pdf}
	\caption[Schematic representation of TAG definitions]{\label{fig:tag-definitions} Schematic representation of TAG definitions. Several genes are represented on a linear chromosome. The red box represent a singleton gene. Orange boxes represent a TAG with two duplicate genes seperated by 7 other genes ($\mathrm{TAG}_7$). Four green boxes constitute a TAG, two genes are, at most, separated by one gene that do not belong to the same family ($\mathrm{TAG}_1$). The two blue boxes represents a TAG with two genes next to each other (there is no spacer: $\mathrm{TAG}_0$). The curved edges represent the homology links between each pair of genes within a TAG.}
\end{figure}

Different methods exist to detect duplicate genes. These methods depend on the type of duplicate genes they target and vary on computation burden as well as in the ease of use (for a review, see \textcite{lallemandOverviewDuplicatedGene2020}).

The common step of all these method is the detection of paralog genes.

\subsubsection{Paralog detection}
Paralogs are homologous genes derived from a duplication event. We can identify them as homologous genes coming from the same genome, or as homologous genes between different species once we filtered out \gls{orthologues} (homologous genes derived from a speciation event).

We can use two gene characteristics to assess the homology between two genes: gene structure or sequence similarity.
The sequence similarity can be tested with a sequence alignment tool, such as \texttt{BLAST}p \autocite{altschulBasicLocalAlignment1990}, \texttt{Psi-BLAST}, and \texttt{HMMER3} \autocite{johnsonHiddenMarkovModel2010}, or \texttt{diamond} \autocite{buchfinkSensitiveProteinAlignments2021}. These tools are heuristic algorithms, which means they may not provide the best results, but do so way faster than exact algorithms, such as the classical Smith and Waterman algorithm \autocite{smithIdentificationCommonMolecular1981} or its optimized versions \texttt{PARALIGN} \autocite{rognesParAlignParallelSequence2001} or \texttt{SWIMM}.

The pipeline proceeds in three steps. First, it estimates the homology links between each pair of genes. Then, it deduces the gene families. Finally, it searches for \gls{TAG}, relying on the position of genes belonging to the same family.
\subsubsection{Estimation of homology links between genes}
This step consists of establishing a homology relationship between each gene in the proteome.

In this step, FTAG-Finder uses \texttt{BLAST} (Basic Local Alignment Search Tool) \autocite{altschulBasicLocalAlignment1990} with an ``all against all'' search on the proteome.

Several \texttt{BLAST} metrics can be used as a homology measure, such as bitscore, identity percentage, E-value or a variation on these.
The choice of metrics can affect the results of graph clustering in the following step, and we should therefore choose them carefully \autocite{gibbonsEvaluationBLASTbasedEdgeweighting2015}.

The user may choose whether to keep a single protein isoform, or to keep all the isoforms in the homology graph. When the user keeps multiple isoforms before the graph clustering step, the isoforms are filtered after, to keep only one family per gene in the graph partition. This possibility allows to detect links between large and small isoforms in the same search. The results are different depending on the choice of the user.

\subsubsection{Identification of gene families}

Based on the homology links between each pair of genes, we construct an undirected weighted graph whose vertices correspond to genes and edges to predicted homology links between them.
We apply a graph clustering algorithm on the gene similarity graph in order to infer the gene families corresponding to densely connected communities of vertices.
FTAG-Finder proposes three alternative algorithms for the graph clustering step: single linkage, Markov Clustering \autocite{vandongenNewClusterAlgorithm1998} or Walktrap \autocite{ponsComputingCommunitiesLarge2005}.
The clustering step allows to predict more homology links:
we assume that genes belonging to the same cluster all share homology links.

\subsubsection{Detection of TAG}

The final step of FTAG-Finder consists in the identification of \gls{TAG} based on the gene families and the positions of genes on the chromosomes.
For a given chromosome, the tool seeks genes belonging to the same family and located close to each other. The tool allows a maximal number of unrelatted genes between the homologous genes. \Cref{fig:tag-definitions} is a schematic representation of some possible \gls{TAG} positioning on a genome associated with their definition in this FTAG-Finder step.
A gene belongs to a TAG of definition \textquote{TAG\textsubscript{$n$}} if no more than $n$ spacer (i.e., genes that do not belong to the same family) are located between them, as depicted in \Cref{fig:tag-definitions}.

\section{Tools}

\subsection{Galaxy}

Galaxy is a workflow manager. It was initially designed to provide an accessible interface to genomics analysis. It consists mostly in a web application offering wrappers around command line tools, associated with a job scheduler running a queue of jobs on the machine hosting the Galaxy instance. \Cref{listing:example-galaxy-wrapper} outlines a sample of a Galaxy tool definition \gls{XML} file for running \texttt{blastp} on a proteome in FASTA format. Similarly to a Snakemake rule, a Galaxy tool \gls{XML} file contains a section for the command to call the wrapped tool (\mintinline{xml}{<command>}). It also contains a section that describes the output files (\mintinline{xml}{<outputs>}). Perhaps more importantly, a section \mintinline{xml}{<inputs>} defines the input files and possible offered tool options. The \mintinline{xml}{<inputs>} section will be rendered as a HTML form in the Galaxy web interface.

Contrary to Snakemake, Galaxy does not infer the succession of jobs based on the input files of the tool \gls{XML} definition file. This role is devoted to the human user, which might be a source of errors, even though Galaxy offers multiple features to help users to ensure the traceabillity of the files produced by the tools. This is arguably a potential shortcoming in an ideal of reproducible science, which might let technically skilled computer users to prefer the approaches proposed by alternatives such as Snakemake or Nextflow (particularly through initiatives like \href{https://nf-co.re/}{nf-core}), which provide useful tool sets to enable bioinformaticians to shrink the human error factors in their data analysis workflows.

\subsection{Snakemake}

Snakemake is a workflow manager first released in 2012.
The user defines rules in a domain specific language built on top of Python.
These rules specify the required inputs and the expected outputs as well as the process that transforms the data.
Based on such rules, Snakemake constructs a \gls{DAG} of jobs from the initial inputs to the desired processed outputs.
\Cref{listing:example-snakemake-rule} depicts a typical Snakemake rule. In this example we focused on a rule to run \texttt{blastp} on a protein FASTA file.

\newpage
\begin{listing}[H]
	\inputminted{xml}{../src/galaxy_simple_wrapper_blastp.xml}
	\caption{\label{listing:example-galaxy-wrapper}
		A simplified example of a Galaxy tool wrapper to run \texttt{blastp}}
\end{listing}
\begin{listing}[H]
	\inputminted{'../src/snakemake_lexer.py:SnakemakeLexer -x'}{../src/snakemake_simple_rule_blastp.smk}
	\caption{\label{listing:example-snakemake-rule}
	A simplified example of a Snakemake rule to run \texttt{blastp}
	}
\end{listing}
\newpage

\chapter{Results}

\section{Update of the Galaxy tools of FTAG-Finder}

The legacy FTAG-Finder scripts were working on an old version of Galaxy, but no longer on the newest Galaxy platform (version 24).

I updated the Python scripts and the Galaxy tool definitions XML files to make them compatible with the Galaxy platform (version 24).

A screenshot of the Galaxy user interface is provided in \Cref{fig:screenshot-galaxy-v24} showing the Find-Homolog tool web form.

The updated scripts and tool definitions are available in the \texttt{galaxy} branch of the git repository (\url{https://gitlab.com/sortion/FTAG-Finder}).

To prove the proper behaviour of the Galaxy tools, I used the \texttt{planemo} tool and wrote automatic test cases.
These tests are written in the XML definition files.

I also installed a Galaxy instance on a QEMU/KVM Debian 12 virtual machine.


\section{Snakemake development and first tests}

I modified the python scripts interfacing with Galaxy to integrate them into a SnakeMake powered pipeline.
Then, I used this pipeline to detect the \Gls{TAG} of \textit{Arabidopsis thaliana}, the Thale cress and \textit{Cyanistes caeruleus}, the Blue Tit.

The code of the Snakemake port of FTAG-Finder is available at \url{https://gitlab.com/sortion/FTAG-Finder}.

A Snakefile is the name of the file containing multiple Snakemake rules, leading to a pipeline definition.

The user defines the parameters of the pipeline in a YAML file, including the name of the run.

The user may launch the pipeline using the standard Snakemake way, specifying the YAML config file:

\begin{minted}{bash}
  $ snakemake --cores all --snakefile=Snakefile --configfile=config.yaml
\end{minted}
The pipeline outputs multiple lists of genes to be used in subsequent statistical analysis on \Gls{TAG}s:

In the following, let us note the name of the run \textquote{\runName}, we will note the name of the files accordingly.

\begin{itemize}
  \item A two-columns file associating each gene to its family identifier: \texttt{results/<name>.<clustering method>.tsv}
		\item A set of random pairs: \texttt{results/lists/<name>/random\_gene\_pairs.tsv};
  \item All pair of genes belonging to the same family: \texttt{results/lists/<name>/intra\_family\_pairs.tsv};
  \item All pair of genes separated by at most $n$ genes, regardless of the family they belong to: \texttt{results/lists/<name>/successive\_gene\_pairs.tsv}; and finally
		\item Pairs of genes belonging to the same \Gls{TAG}: \texttt{results/lists/<name>/local\_gene\_pairs.tsv}.
\end{itemize}

These outputs enable us to perform multiple statistical tests on \Gls{TAG}.

From the gene to family mapping file, I plotted the distribution of family size for two distant Eukaryotes, the Thale cress plant and the Blue Tit in \Cref{fig:distribution-family-size} to check that the result matches the expectation.

\fladdfig{
	\begin{subfigure}{0.4\textwidth}
		\includegraphics[width=\columnwidth]{../media/plots/gene_families_size_distribution_TAIR10 [mcl].pdf}
		\caption{\textit{Arabidopsis thaliana}}
	\end{subfigure}
	\begin{subfigure}{0.4\textwidth}
		\includegraphics[width=\columnwidth]{../media/plots/gene_families_size_distribution_CyaCae2 [mcl].pdf}
		\caption{\textit{Cyanistes caeruleus}}
	\end{subfigure}
	\caption[Distribution of gene family sizes]{\label{fig:distribution-family-size}Distribution of gene family size. The $x$-axis corresponds to the number of genes per family, and the $y$-axis corresponds to the number of family falling into this category, in \textit{Arabidopsis thaliana} (a) and \textit{Cyanistes caeruleus} (b). The gene families are inferred with a \gls{MCL} of the similarity graph of the proteins. The homology links used in this similarity graph have been estimated using the bitscore of the local alignment using blastp on the longest protein isoform.
	}
}

%\fladdfig{

\begin{figure}[H]
	\begin{subfigure}{0.4\textwidth}
		\includegraphics[width=\columnwidth]{../media/img/plantnet/1021430140.png} % https://identify.plantnet.org/users/102417086#observations
		\caption{\label{fig:arabidopsis-thaliana-plantnet}\textit{Arabidopsis thaliana}}
	\end{subfigure}
	\begin{subfigure}{0.4\textwidth}

		\includegraphics[width=\columnwidth]{../media/img/cyaCae_Courcouronnes.jpg}
		\caption{\label{fig:cyanistes-caeruleus}\textit{Cyanistes caeruleus}}
	\end{subfigure}
	\caption[Photos of two relatively distant Eukaryotes]{\label{fig:species-pictures}
	  Photos of two relatively distant Eukaryotes.
		(a) A Thale cress, \textit{Arabidopsis thaliana}, in the street at Clermont-Ferrand, France (CC-By-SA Fabrice Rubio via \href{https://plantnet.org/}{PlantNet}\footnote{PlantNet ref. : \url{https://identify.plantnet.org/k-southwestern-europe/observations/1021430140}}).
		(b) An Eurasian Blue Tit, \textit{Cyanistes caeruleus}, foraging for insects in bulrushes at Courcouronnes, France (own work).
	}%
	\end{figure}
% }

	\begin{table}
		\centering
		\begin{tabular}{ccc}
			\toprule
			Species & Families & Duplicate genes \\
			\midrule
			\textit{Arabidopsis thaliana} & 5379 & 17784 \\
			\textit{Cyanistes caeruleus} & 2282 & 6492 \\
			\bottomrule
		\end{tabular}
		\caption{\label{tab:family-gene-counts}Predicted families and gene counts for \textit{Arabidopsis thaliana} and \textit{Cyanistes caeruleus}}
	\end{table}


	\section{Biological analyses}

	\subsection{Gene orientation concordance}

	% 1. Question / Visual representation
	% 2. Statistical test
	% 3. Conclusion

	Based on the \gls{TAG} listed by the FTAG-Finder pipeline, we wanted to check wether the genes belonging to the same \gls{TAG} shared the same orientation.
	We distinguish three cases: the orientation of two genes is considered as coherent, when they belong to the same strand, divergent when they head to the opposite strand, and convergent otherwise. \Cref{fig:orientation-convergence-scheme} depicts the three cases.
	\begin{figure}
	  \centering
	  \includegraphics{../figures/gene_orientation_figure.pdf}
		\caption[Schematic representation for the orientation convergence test]{\label{fig:orientation-convergence-scheme}Schematic representation of the three cases we consider for the orientations  coherence test: coherent orientation in orange, convergent orientation in blue and divergent orientation in green}
	\end{figure}

	\begin{figure}
	  \centering
	  \includegraphics[width=0.5\columnwidth]{../media/plots/20240529_concordance_orientation_TAIR10.pdf}
  \caption[Concordance of DNA strand of pairs of genes within the same family or not in TAIR10]{
	Concordance of DNA strand of pairs of genes within the same family or not in TAIR10.
	Coherent genes are genes located on the same strand. Convergent and divergent gene pairs are located on different DNA strands: they are facing either towards the other or at the opposite direction. The proportion of concordance state is shown for two set of pair of genes located one next to the others with no spacer between them on the same chromosome and that are either the member of the same family (Local pairs) or not (Successive pairs). }
\end{figure}

To test wether a concordance is privileged we perform a Fisher exact test on the contingency table reported in \Cref{tab:fisher-contingency-table}:

Let us denote by PST the pairs of successive genes belonging to a TAG and PSnT the pair of successive genes that does not belong to a TAG, following the convention initiated by \cite{le-hoangEtudeTranscriptomiqueGenes2017}.

The hypotheses involved in the test are the following:

\begin{cases}
	(H_0) & \text{the proportion of each orientation concordance is similar in both groups} \\
	(H_1) & \text{the proportion is different between PST and PSnT}
\end{cases}


\begin{table}
	\centering
	\begin{tabular}{cccc}
		\hline
		& coherent & convergent & divergent \\
		\hline
		PST & 1516 & 50 & 64 \\
		PSnT & 12498 & 4913 & 4916 \\
		\hline
	\end{tabular}
	\caption{\label{tab:fisher-contingency-table}Orientation concordance Fisher exact test contingency table on TAIR10.}
\end{table}

The $p$-value of the Fisher test is below $2.2 \cdot 10^{-16}$, so at level $\alpha = 0.05$, we reject $(H_{0})$:
the proportion of convergent genes in TAG or not TAG differs. Moreover, the orientation of genes within a \gls{TAG} is most often coherent.

We have found a result similar to \cite{le-hoangEtudeTranscriptomiqueGenes2017} from which we borrowed the idea of the analysis.

We may interpret this result as follows: it looks like there is a selective pressure on the orientation of the genes,  that keeps the copies of the genes within a TAG in the same orientation. This is maybe to share a similar gene regulation system.

\chapter{Discussion and perspectives}

The first tests demonstrates that my Snakemake version of FTAG-Finder is working.

To confirm the correct behavior of my development, we could make a comparison of results obtained with Snakemake and those performed by Galaxy.

It would be required also to test the proper functionality of my work on the compute machines of the LaMME laboratory.

An evaluation of the performance of the tool might be interesting.

Based on the different lists of genes generated by this version of FTAG Finder, we could perform more statistical tests on TAG.
For instance, we could perform an analysis of overrepresentation of particular Gene Ontology terms for genes belonging to a TAG as compared with the other genes.

We could also perform an analysis on the age of the duplication of the genes of the TAGs by doing an analysis of the Ka/Ks ratio.
The Ka/Ks ratio corresponds to the number of nonsynonymous substitutions per non synonymous site $(K_{a})$ over the number of synonymous substitutions per synonymous site $(K_{s})$.

The Thale cress lives in very diverse habitat. Duplicate genes offers it a range of possibility to evolve and develop resistance to the sometimes polluted environment.
Gene duplication enables the Thale cress to adapt more rapidly to changes in its environment and they often contains genes involved in the response to stress.


	% TODO: Thale cress: duplicate genes corresponds to gene related to stress
	% Arabidopsis lives worldwide in diverse environment, sometimes polluted as in street faults.
	% Gene duplication enables the Thale cress adapt more rapidly to changes in its environment.


\chapter{Conclusion}

During my internship, I updated the Python scripts and the tool definitions so that they become compatible with the latest Galaxy version (v24) I successfully ported the Galaxy version of FTAG-Finder on Snakemake. I ran this pipeline on the IFB compute cluster on the proteome of \textit{Arabidopsis thaliana}, \textit{Drosophila melanogaster}, and \textit{Cyanistes caeruleus}.

Using the list of genes outputed by the FTAG-Finder pipeline, I performed a Fisher exact test to check wether the TAG genes shared the same orientation.

Throughout this internship I extended my knowledge on Snakemake. I discovered the Galaxy tool.

During my internship, I developed a stronger knowledge on duplicate genes and on the method used in their analysis.

I enjoyed the multiple seminar that took place in the LaMME laboratory, which offered me an overview on diverse research topics.

\printbibliography[title=References] %

\begin{appendices}
	\chapter{Annex}

	\section{Contributions to FTAG-Finder}

	\subsubsection{FTAG-Finder on Snakemake}
	I wrote the Snakemake version of FTAG-Finder. Doing so, I refactored a little bit the python scripts originally written for Galaxy by predecessor interns. These refactoring consisted mostly in offering a more versatile command line interface.


	\subsubsection{Additional scripts}

	The version of FTAG-Finder I inherited from predecessor interns required some input files to be manually crafted.
	I wrote several scripts that allows the Snakemake version of the pipeline to run from the released proteome file in fasta format and the feature annotations in \gls{GFF3} format down to the end results without manual edit.

	These scripts consists in:
	\begin{itemize}
		\item A script to extract a protein identifier to gene identifier mapping from some standard FASTA header formats (i. e. TAIR, Ensembl and Flybase). This selection is obviously incomplete, but the scripts written may serve as a basis to support other headers conventions.

		\item A script that extracts the relevant parts of the \gls{GFF3} files containing positions of genomic features (genes, non coding RNA, chromosomes, etc.). The Find Homologs step previously required manual edit of the file.

	\end{itemize}

	\subsubsection{Creation of special lists of genes}
	Fabien Jasmin described several possible statistical analyses that could be performed on Tandemly Arrayed Genes and special list of genes that would be required for such analyses \cite{jasminStudyTandemlyArrayed2016}. Kévin Normand then wrote a python script that implement the creation list module and integrated this tool in the LaMME Galaxy instance \cite{kevinDevelopmentCreationListTool2017}. Building on top of these works, I (re)wrote python scripts for the \textquote{Creation List} module, with modifications to better fit my Snakemake version of the pipeline, and my own taste. Specifically, I splitted the different creation list scripts into their own Python scripts, and I wrote a bespoke \gls{CLI} for each of them using the standard Python \texttt{argparse} module.

	\section{Supplementary figures}

	\begin{figure}
	\includegraphics[width=0.8\textwidth]{../media/snakemake/filegraph.pdf}
	\caption[An example of DAG built by Snakemake on the FTAG-Finder pipeline]{
		\label{fig:ftag-finder-filegraph}
		A example of \gls{DAG} built by Snakemake on the FTAG-Finder pipeline. The final rule `all' defines the desired final output files. Then, Snakemake infers the succession of jobs based on the input and output files defined in the rules. Based on this graph, the rules are either executed one after the other when a rule depends on the other or in parallel when the rules are independent.
	  }
	\end{figure}

	\begin{figure}
	  \centering
	  \includegraphics[width=0.9\textwidth]{../media/screenshots/Screenshot_20240907_103817.png}
	  \caption{\label{fig:screenshot-galaxy-v24}Screenshot of the Find-Homologs tools under Galaxy v24 web form}
	  \end{figure}

\end{appendices}

\flstop%

\end{document}
% LocalWords:  iform acyclic bioinformaticians traceabillity
